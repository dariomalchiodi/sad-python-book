#! /usr/bin/env python
# -*- coding: utf-8 -*-

import json
import re
import sys

prog = re.compile(r'^\s*(#+.*)')
header_prefix = 'h-'
next_header_id = 0
toc = []
in_inline_code = False

flatten = lambda l: [item for sublist in l for item in sublist]

def convert_title(notebook_title):
    return notebook_title[4:].replace('_', ' ')

def parse_header(header, notebook_name):
    header = header.strip()
    i = 0
    while header[i] == '#':
        i += 1
    return (i,
            notebook_name,
            '{}{}'.format(header_prefix, next_header_id),
            header[i:].strip() + '\n')

def process_row(row, notebook_name):
    global next_header_id, toc, in_inline_code

    if row[:8] == '<div id=':
        row = row[row.index('\n')+1:]

    if row[:3] == "```":
        in_inline_code = not in_inline_code

    if not in_inline_code:
        result = prog.match(row)
        if result:
            toc_entry = parse_header(row, notebook_name)
            toc.append(toc_entry)
            row = row.strip() + '\n'
            row = '<div id="{}{}"></div>\n\n'.format(header_prefix,
                                                     next_header_id) + row
            next_header_id += 1
    return row

def process_cell(cell, notebook_name):
    if cell['cell_type'] == 'markdown':
        cell['source'] = [process_row(row, notebook_name)
                          for row in cell['source']]
    return cell

title = 'Superhero data science'
volume = u'Vol 1: probabilità e statistica'.encode('utf-8')

def process_notebook(notebook_name, notebook_dir):
    global next_header_id, in_inline_code

    print 'processing ' + notebook_name

    first_cell = {
      "cell_type": "markdown",
      "metadata": {'header': True},
      "source": [
        "<div class=\"header\">\n",
        "D. Malchiodi, {}. {}: {}.\n".format(title,
                                             volume,
                                             convert_title(notebook_name)),
        "</div>\n",
        "<hr style=\"width: 90%;\" align=\"left\" />"
      ]
    }

    last_cell = {
      "cell_type": "markdown",
      "metadata": {'footer': True},
      "source": [
        "<hr style=\"width: 90%;\" align=\"left\" />\n",
        "<span style=\"font-size: 0.8rem;\">D. Malchiodi, {}. {}: {}, 2017.</span>\n".format(title, volume, convert_title(notebook_name)),
        "<br>\n",
        "<span style=\"font-size: 0.8rem;\">Powered by <img src=\"img/jupyter-logo.png\" style=\"height: 1rem; display: inline; margin-left: 0.5ex; margin-top: 0;\" alt=\"Jupyter Notebook\"></span>\n",
        "<div style=\"float: left; margin-top: 1ex;\">\n",
        "<img src=\"http://mirrors.creativecommons.org/presskit/icons/cc.large.png\" style=\"width: 1.5em; float: left; margin-right: 0.6ex; margin-top: 0;\">\n",
        "<img src=\"http://mirrors.creativecommons.org/presskit/icons/by.large.png\" style=\"width: 1.5em; float: left; margin-right: 0.6ex; margin-top: 0;\">\n",
        "<img src=\"http://mirrors.creativecommons.org/presskit/icons/nc.large.png\" style=\"width: 1.5em; float: left; margin-right: 0.6ex; margin-top: 0;\">\n",
        "<img src=\"http://mirrors.creativecommons.org/presskit/icons/nd.large.png\" style=\"width: 1.5em; float: left; margin-right: 0.6ex; margin-top: 0;\">\n",
        "<span style=\"font-size: 0.7rem; line-height: 0.7rem; vertical-align: middle;\">Quest'opera è distribuita con Licenza <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-nc-nd/4.0/\">Creative Commons Attribuzione - Non commerciale - Non opere derivate 4.0 Internazionale</a></span>.\n",
        "</div>"
      ]
    }

    next_header_id = 0
    in_inline_code = False

    with open('{}/{}.ipynb'.format(notebook_dir,
                                   notebook_name)) as json_file:
        notebook = json.load(json_file)
    old_cells = [cell for cell in notebook['cells'] \
                 if 'header' not in cell['metadata'] \
                 and 'footer' not in cell['metadata']]
    new_cells = [process_cell(cell, notebook_name)
                 for cell in old_cells]
    notebook['cells'] = [first_cell] + new_cells + [last_cell]
    with open('{}/{}.ipynb'.format(notebook_dir, notebook_name),
              'w') as outfile:
        json.dump(notebook, outfile)

def process_notebooks(notebooks, notebook_dir):
    global toc

    toc = []

    for notebook_name in notebooks:
        process_notebook(notebook_name.encode('utf-8'), notebook_dir)

def to_html_entry(row):
    html_toc_line = ('<h{header}>'
                     '<a href="{file}.html#{anchor}">{name}</a>'
                     '</h{header}>')
    return html_toc_line.format(header=row[0],
                                file=row[1],
                                anchor=row[2],
                                name=row[3].encode('utf-8'))

def to_html_toc(title, volume, author):

    head_html = """
<!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">

    <title>Superhero data science, vol. 1</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="custom.css">
    <link rel="shortcut icon" type="image/x-icon" href="img/superhero-favicon.ico" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
      body { font-family: 'Alegreya', serif; }
      h2 { margin-left: 0.5cm !important;}
      h2.title { margin-left: 0 !important; }
      h3 { margin-left: 1cm !important;}
      h4 { margin-left: 1.5cm !important;}
      h5 { margin-left: 2cm !important;}
      h6 { margin-left: 2.5cm !important;}
      p, li { font-size: 1rem; }
                                                                                </style>
  </head>"""

    start_body="""
<body>
  <div class="container">
    <div class="row">
      <div style="float: right;">
      <img style="width: 150px; margin-top: 1em;"
           src="img/superhero-icon.png"/>
      </div>
      <div class="page-header">
        <h1 style="font-size: 3rem; font-weight: 900;">{}</h1>
        <h1 style="font-size: 2.3rem; font-weight: 600;">{}</h1>
        <h1 style="font-size: 2rem; font-weight: 600;">{}</h1>
      </div>
    </div>
    <div class="row">
      <p>Questa raccolta di dispense è a uso dei miei studenti
      dell'insegnamento di <a href="http://malchiodi.di.unimi.it/teaching/data-analytics">Statistica e analisi dei dati</a>. Gli argomenti trattati
      riguardano i temi non presenti sul libro di testo adottato e
      sono quindi da intendersi come materiale integrativo (da leggere:
      <b>non</b> sostitutivo del libro di testo!).</p>
      <p>Le dispense possono essere utilizzate in tre modi differenti:
      leggendo la versione statica tramite browser, utilizzando una versione
      interattiva basata su Docker oppure clonando un repository github.</p>
      <h2 class="title">Versione statica</h2>
      <p>Nella loro versione statica le dispense sono consultabili tramite
      un browser Web (in versione recente) collegandosi all'indirizzo
      <a href="https://dariomalchiodi.gitlab.io/sad-python-book/">https://dariomalchiodi.gitlab.io/sad-python-book</a>.</p>
      <h2 class="title">Versione interattiva basata su Docker</h2>
      <p>Il contenuto delle dispense è scritto utilizzando dei
      <a href="http://www.jupyter.org">notebook jupyter</a>, e quindi è
      possibile eseguire e modificare il codice in esse contenuto. In
      particolare, la versione basata su
      <a href="http://www.docker.com">Docker</a> non richiede di effettuare
      alcuna installazione (a parte ovviamente quella di Docker). Per
      utilizzare le dispense è sufficiente, nell'ordine:
      <ol>
        <li>Aprire un terminale (aka prompt dei comandi) e posizionarsi
        nella directory in cui si vuole tener traccia del proprio
        lavoro;</li>
        <li>Eseguire il comando <tt>docker pull dariomalchiodi/sad</tt> al
        fine di scaricare, se serve, la versione più aggiornata del
        materiale;</li>
        <li>Eseguire il comando <tt>docker run --rm -ti -p 127.0.0.1:8888:8888 -v $(pwd):/home/jovyan/my-work dariomalchiodi/sad</tt>;</li>
        <li>Analizzare l'output ottenuto e da esso copiare il testo <tt>http://localhost:8888/?token=TOKEN</tt>, dove <tt>TOKEN</tt> rappresenta una
        successione di caratteri alfanumerici;</li>
        <li>Incollare il testo copiato nella barra indirizzi del proprio
        browser.</li>
      </ol>
      <h2 class="title">Versione interattiva basata su git</h2>
      <p>Chi preferisce cimentarsi con l'installazione degli strumenti
      utilizzati (e i miei studenti dovrebbero sentire l'impulso di
      farlo...) può scaricare e utilizzare i soli notebook clonando il
      repository
      <a href="https://gitlab.com/dariomalchiodi/sad-python-book">git@gitlab.com:dariomalchiodi/sad-python-book.git</a>. Per verificare quale software
      installare è sufficiente analizzare il contenuto del file
      <tt>Dockerfile</tt> all'interno del repository stesso.
      <hr />

""".format(title, volume, author)

    end_body = """
    </div>

    <hr style="width: 90%;" align="left" />
    <span style="font-size: 0.8rem;">{}, {}. {}, 2017.</span>
    <br>
    Powered by <img src="img/jupyter-logo.png" style="height: 1rem; display: inline; margin-left: 0.5ex;" alt="Jupyter Notebook">
    <div style="float: left; margin-top: 1em;">
    <img src="http://mirrors.creativecommons.org/presskit/icons/cc.large.png" style="width: 1.5em; float: left; margin-right: 0.6ex; margin-top:0;">
    <img src="http://mirrors.creativecommons.org/presskit/icons/by.large.png" style="width: 1.5em; float: left; margin-right: 0.6ex; margin-top:0;">
    <img src="http://mirrors.creativecommons.org/presskit/icons/nc.large.png" style="width: 1.5em; float: left; margin-right: 0.6ex; margin-top:0;">
    <img src="http://mirrors.creativecommons.org/presskit/icons/nd.large.png" style="width: 1.5em; float: left; margin-right: 0.6ex; margin-top:0;">
    <span style="font-size: 0.7rem; line-height: 0.7rem; vertical-align: middle;">Quest'opera è distribuita con Licenza <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribuzione - Non commerciale - Non opere derivate 4.0 Internazionale</a></span>.
    </div>
  </div>

</body>
</html>
""".format(author, title, volume)

    mid_body = '\n'.join([to_html_entry(row) for row in toc])
    return head_html + start_body + mid_body + end_body

def to_ipynb_entry(row):
    entry = {
        "cell_type": "markdown",
        "metadata": {},
        "source": []
    }

    prefix = '#'*row[0]

    url = '{name}.ipynb#{anchor}'.format(prefix=prefix,
                                                 name=row[1],
                                                 anchor=row[2])

    entry['source'].append('{prefix} [{text}]({url})'.format(prefix=prefix,
                                                             text=row[3].encode('utf-8'),
                                                             url=url))
    return entry

def to_ipynb_toc(title, author):

    toc_notebook = {
     "metadata": {
      "kernelspec": {
       "display_name": "Python 3",
       "language": "python",
       "name": "python3"
      },
      "language_info": {
       "codemirror_mode": {
        "name": "ipython",
        "version": 3
       },
       "file_extension": ".py",
       "mimetype": "text/x-python",
       "name": "python",
       "nbconvert_exporter": "python",
       "pygments_lexer": "ipython3",
       "version": "3.6.2"
      }
     },
     "nbformat": 4,
     "nbformat_minor": 2
    }

    toc_header = [{
        "cell_type": "markdown",
        "metadata": {},
        "source": ['# {}\n'.format(title),
                   '### {}\n'.format(author)]
    }]

    toc_notebook['cells'] = toc_header + [to_ipynb_entry(t) for t in toc]

    return toc_notebook

if len(sys.argv) != 2:
    print('usage: {} metadata-file'.format(sys.argv[0]))
else:
    with open(sys.argv[1]) as json_file:
        metadata = json.load(json_file)

    notebook_dir = metadata['notebook_dir']
    process_notebooks(metadata['chapters'], notebook_dir)

    with open('index.html', 'w') as html_toc:
        html_toc.write(to_html_toc('Superhero data science',
                                   u'Vol 1: probabilità e statistica'.encode('utf-8'),
                                   'D. Malchiodi'))

    with open('{}/index.ipynb'.format(notebook_dir), 'w') as ipynb_toc:
        json.dump(to_ipynb_toc(metadata['title'], metadata['author']),
                                                  ipynb_toc)
