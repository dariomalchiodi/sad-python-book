import scrapy
import pandas as pd
import random
import numpy as np

def sanitize(value):
    return value if value and value != '-' else ''

class SuperheroesSpider(scrapy.Spider):
  name = "superheroes"

  start_urls = [ 'https://www.superherodb.com/characters/', ]

  def parse(self, response):
    names = response.css('li.char-li a::attr(href)').extract()
    for n in names:
      yield response.follow('https://www.superherodb.com/' + n,
                            self.parse_hero)

  def xpath_sibling(self, response, tag, label):
      xpath = '//{}[text()="{}"]/following-sibling::{}'.format(tag,
                                                               label, tag)
      return (response.xpath(xpath)
                      .css('::text'))

  def parse_hero(self, response):
    name = response.css('h1::text').extract_first()
    identity = response.css('h2::text').extract_first()
    birth_place = (self.xpath_sibling(response, 'td', 'Place of birth')
                       .extract_first())
    publisher = (self.xpath_sibling(response, 'td', 'Publisher')
                     .extract_first())
    height = (self.xpath_sibling(response, 'td', 'Height')
                  .re(r'.*//\s(\d+).*'))
    height = int(height[0])
    if height:
        height += np.round(random.random(), 2)

    weight = (self.xpath_sibling(response, 'td', 'Weight')
                  .re(r'.*//\s(\d+).*'))
    weight = int(weight[0])
    if weight:
        weight += np.round(random.random(), 2)
    gender = (self.xpath_sibling(response, 'td', 'Gender')
                  .extract_first())[0]
    first_app = (self.xpath_sibling(response, 'td', 'First appearance')
                     .re(r'.*\(.*(\d\d\d\d)\).*'))
    eye_color = (self.xpath_sibling(response, 'td', 'Eye color')
                     .extract_first())
    hair_color = (self.xpath_sibling(response, 'td', 'Hair color')
                      .extract_first())
    strength = (self.xpath_sibling(response, 'div', 'Strength')
                    .extract_first())
    intelligence = (self.xpath_sibling(response, 'div', 'Intelligence')
                        .extract_first())
    intelligence_labels = ('low', 'moderate', 'average', 'good', 'high')
    if intelligence and intelligence != '-':
        intelligence = int(intelligence)
        intelligence = intelligence_labels[(intelligence-1) / 20]
    else:
        intelligence = ''

    yield {'name': name,
           'identity': sanitize(identity),
           'birth_place': sanitize(birth_place),
           'publisher': sanitize(publisher),
           'height': sanitize(height),
           'weight': sanitize(weight),
           'gender': sanitize(gender),
           'first_appearance': sanitize(first_app),
           'eye_color': sanitize(eye_color),
           'hair_color': sanitize(hair_color),
           'strength': sanitize(strength),
           'intelligence': sanitize(intelligence)}
